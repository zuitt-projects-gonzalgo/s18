//javascript objects
	//unlike array that uses index, objects use properties
	/*
		syntax:
		let objectName = {
			key: value,
			key: (function),
			key: object {
				key: value
			},
			key: [array]
		}
	*/
let cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
};

console.log(cellphone);
console.log(typeof cellphone);

	//creating objects using constructor function
		/*
			syntax:
			function objectName(keyA, keyB) {
				this.keyA = keyA;
				this.keyB = keyB;
			};
		*/
	function Laptop(name, manufacturedDate) {
		this.name = name;
		this.releasedDate = manufacturedDate;
	}

		//new operator creates an instance of an object
	console.log(new Laptop("Lenovo", 2008));

	console.log(Laptop("Asus", 2010));
		//result: undefined

	//creating empty objects
	let computer = {};
	console.log(computer);
	let myComputer = new Object();
	console.log(myComputer);

	//accessing array objects
	let machines = [cellphone];
		//access the property of an object inside an array
		console.log(machines[0].name);
		console.log(machines[0]["name"]);

	//initializing/ adding/ deleting/ reassigning object properties
	let car = {};
		//initialing/adding obeject properties using a dot (.) and [] notation
	car.name = "Honda Civic";
	console.log(car);
	car["manufacturedDate"] = 2019;
	console.log(car);

		//deleting object propeties
	delete car["name"];
	console.log(car);

	//mini-activity
	car.brand = "Toyota";
	car.model = "Avalon";
	car.color = "Red";
	car.location = "Cubao";
	console.log(car);

	//reassigning object properties
	car.manufacturedDate = 2022;
	console.log(car);

//object methods
	//a method is a function which is a property of an object
	//they are also functions and one of the key differences they have is that methods are functions related to a specific object
	//methods are defined based on what an object is capable of doing and how it should work

let person = {
	name: "Jin",
	talk: function() {
		console.log(`Hello my name is ${this.name}`);
	},
}

person.talk();

let friend = {
	firstName: "Lisa",
	lastName: "Manobal",
	address: {
		country: "South Korea",
		city: "Seoul"
	},
	emails: ["lisa@mail.com", "lm@mail.com"],
	indroduce: function() {
		console.log(`Hello my name is ${this.firstName} ${this.lastName}, I live in ${this.address.city}. My personal email is ${this.emails[0]}`);
	},
};
friend.indroduce();

//real world application
	//scenario:
	//1. create a game that would have several pokemons to interact with each other
	//2. every pokemon should have, stats, properties, functions

	let myPokemon = {
		name: "Bulbasaur",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function() {
			console.log(`${this.name} takled another pokemon`);
			console.log(`targetPokemon's health is now reduced`);
		},
		faint: function() {
			console.log(`${this.name} fainted`);
		}
	};

	myPokemon.tackle();
	myPokemon.faint();

	//creating obeject with an object constructor
	function Pokemon(name, lvl, hp) {
		//properties
		this.name = name;
		this.lvl = lvl;
		this.health = hp * 2;
		this.attack = lvl;

		//methods
		this.tackle = function(target) {
			console.log(`${this.name} tackled ${target.name}`);
			console.log(`targetPokemon's health is now reduced`);
			console.log(target.name);
			console.log(this.name);
		}
		this.faint = function() {
			console.log(`${this.name} fainted`);
		}
	};

	//creat new instances of the pokemon object eahc with their unique properties
	let pikachu = new Pokemon("Pikachu", 3, 50);
	let ratata = new Pokemon("Ratata", 5, 25);

	//providing the "ratata" object as an argument to "pikachu" tackle method will create interaction
	pikachu.tackle(ratata);
	ratata.tackle(pikachu);